ETCDIR=${DESTDIR}/etc
DINITDDIR=${ETCDIR}/dinit.d
BOOTDDIR=${DINITDDIR}/boot.d
SCRIPTSDIR=${DINITDDIR}/scripts
STATEDIR=${DESTDIR}/var/lib/dinit
MODE=644
SCRIPTMODE=755
DIRMODE=755

all: links environment

install: all

dirs:
	install -d -m ${DIRMODE} ${DINITDDIR}
	install -d -m ${DIRMODE} ${BOOTDDIR}
	install -d -m ${DIRMODE} ${SCRIPTSDIR}
	install -d -m ${DIRMODE} ${STATEDIR}

environment: dirs
	install -m ${MODE} environment ${DINITDDIR}

scripts: dirs
	install -m ${SCRIPTMODE} dinit.d/scripts/early-filesystems.sh ${SCRIPTSDIR}
	install -m ${SCRIPTMODE} dinit.d/scripts/filesystems.sh ${SCRIPTSDIR}
	install -m ${SCRIPTMODE} dinit.d/scripts/modules.sh ${SCRIPTSDIR}
	install -m ${SCRIPTMODE} dinit.d/scripts/rcboot.sh ${SCRIPTSDIR}
	install -m ${SCRIPTMODE} dinit.d/scripts/rootfscheck.sh ${SCRIPTSDIR}

services: dirs scripts
	install -m ${MODE} dinit.d/auxfscheck ${DINITDDIR}
	install -m ${MODE} dinit.d/boot ${DINITDDIR}
	install -m ${MODE} dinit.d/live ${DINITDDIR}
	install -m ${MODE} dinit.d/early-filesystems ${DINITDDIR}
	install -m ${MODE} dinit.d/filesystems ${DINITDDIR}
	install -m ${MODE} dinit.d/hwclock ${DINITDDIR}
	install -m ${MODE} dinit.d/loginready ${DINITDDIR}
	install -m ${MODE} dinit.d/modules ${DINITDDIR}
	install -m ${MODE} dinit.d/rcboot ${DINITDDIR}
	install -m ${MODE} dinit.d/recovery ${DINITDDIR}
	install -m ${MODE} dinit.d/rootfscheck ${DINITDDIR}
	install -m ${MODE} dinit.d/rootrw ${DINITDDIR}
	install -m ${MODE} dinit.d/rootshell ${DINITDDIR}
	install -m ${MODE} dinit.d/single ${DINITDDIR}
	install -m ${MODE} dinit.d/tty1 ${DINITDDIR}
	install -m ${MODE} dinit.d/tty1-autologin ${DINITDDIR}
	install -m ${MODE} dinit.d/tty2 ${DINITDDIR}
	install -m ${MODE} dinit.d/tty2-autologin ${DINITDDIR}
	install -m ${MODE} dinit.d/tty3 ${DINITDDIR}
	install -m ${MODE} dinit.d/tty4 ${DINITDDIR}
	install -m ${MODE} dinit.d/udev-settle ${DINITDDIR}
	install -m ${MODE} dinit.d/udev-trigger ${DINITDDIR}

links: services
	ln -sf ../modules ${BOOTDDIR}/modules
	ln -sf ../modules ${BOOTDDIR}/modules

uninstall:
	rm -rf ${DINITDDIR}

.PHONY: all install dirs environment scripts services links uninstall
