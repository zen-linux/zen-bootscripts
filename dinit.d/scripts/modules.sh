#!/usr/bin/sh

# Check that the kernel has module support.
[ -e /proc/ksyms -o -e /proc/modules ] || exit 0

MODULES_DIR=/etc/modules.d

case "${1}" in
    start)
        if [ ! -d "$MODULES_DIR" ]; then
            exit 0;
        fi

        for MODULE in "$MODULES_DIR"/*; do
            MODULE=$(basename $MODULE)

            if [ "$MODULE" != '*' ]; then
                # Try to load the module with its arguments
                modprobe "$MODULE" > /dev/null
            fi
        done
        ;;
    *)
        echo "Usage: ${0} {start}"
        exit 1
        ;;
esac
