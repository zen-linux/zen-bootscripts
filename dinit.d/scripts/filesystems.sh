#!/usr/bin/sh

set -e

if [ "$1" != "stop" ]; then

  echo "Mounting auxillary filesystems...."
  #/usr/bin/mount -t tmpfs -o nodev,nosuid tmpfs /dev/shm
  #/usr/bin/mount -t devpts -o gid=root devpts /dev/pts # TODO change gid back to tty
  /usr/bin/mount -avt noproc,nonfs

  #/usr/bin/swapon /swapfile

fi;
