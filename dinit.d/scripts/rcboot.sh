#!/usr/bin/sh

set -e

umask 0077

if [ "$1" != "stop" ]; then

  # cleanup
  # (delete /tmp etc)
  rm -rf /tmp/* /tmp/.[!.]* /tmp/..?*

  # empty utmp, create needed directories
  : > /var/run/utmp
  mkdir -m og-w /var/run/dbus

  # Configure random number generator
  if [ -e /var/lib/dinit/random-seed ]; then
    #echo "Restoring random number seed..."
    cat /var/lib/dinit/random-seed > /dev/urandom;
  fi

  #echo "myhost" > /proc/sys/kernel/hostname

  # custom keyboard map
  loadkeys de_CH-latin1

else

  # The system is being shut down

  if [ -w /var/lib/dinit  ]; then
    #echo "Saving random number seed..."
    POOLSIZE="$(cat /proc/sys/kernel/random/poolsize)"
    dd if=/dev/urandom of=/var/lib/dinit/random-seed bs="$POOLSIZE" count=1 2> /dev/null
  fi

fi;
